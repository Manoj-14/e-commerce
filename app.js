$(document).ready(function () {
  $("#logo").click(function () {
    if ($(window).width() < 650) {
      $(".nav-links").slideToggle();
    }
  });
  $(".content").fadeIn(800);
  $(".content").hover(function () {
    $("#sub-title").css("visibility", "visible");
  });
  $(".signup-btn").click(function () {
    if ($(".login-form").css("display") == "flex") {
      $(".login-form").hide();
      $(".signup-form").css("display", "flex");
    }
  });
  $(".login-btn").click(function () {
    if ($(".signup-form").css("display") == "flex") {
      $(".signup-form").hide();
      $(".login-form").css("display", "flex");
    }
  });
});
